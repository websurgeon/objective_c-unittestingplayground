# Unit Testing Playground

This is a project to tryout and document different unit testing techniques that can be used in iOS projects.

Many of the ideas used in this project come from the following resources:

Jon Reid - Quality Coding [http://qualitycoding.org/](http://qualitycoding.org/)  
Bernd Rabe - RABE_IT Services blog [http://www.berndrabe.de/blog/](http://www.berndrabe.de/blog/)


### How to add a unit testing target to an existing XCode project

1. Create a new target (UnitTestingPlaygroundTests)

        File > New > Target... : iOS : Other : Cocoa Touch Unit Testing Bundle

2.  Add main target as a target dependancy

        Project Navigator : Targets : UnitTestingPlaygroundTests > Build Phases

        Target Dependancies : + : UnitTestingPlayground

3.  Set Build Settings

        Project Navigator : Targets : UnitTestingPlaygroundTests > Build Settings

        Bundle Loader = $(BUILT_PRODUCTS_DIR)/UnitTestingPlayground.app/UnitTestingPlayground
        Test Host = $(BUNDLE_LOADER)
        Symbols Hidden by Default = NO

4. Set Testing Target to run when Scheme Tests are run

        Product > Scheme > UnitTestingPlayground
        Product > Scheme > Edit Scheme...
    
        Select 'Test' and add the UnitTestingPlaygroundTests to run as part of the scheme

        Press Ctrl+U to Check that Tests run ok
        
### Test Driven Development (TDD)

#### Red, Green, Refactor

1. Write a failing test
2. Write the least amount of production code to pass the test
3. Refactor (test code and production code)


### The MIT License

> Copyright (C) 2013 Peter Barclay.
>
> Permission is hereby granted, free of charge, to any person
> obtaining a copy of this software and associated documentation files
> (the "Software"), to deal in the Software without restriction,
> including without limitation the rights to use, copy, modify, merge,
> publish, distribute, sublicense, and/or sell copies of the Software,
> and to permit persons to whom the Software is furnished to do so,
> subject to the following conditions:
>
> The above copyright notice and this permission notice shall be
> included in all copies or substantial portions of the Software.
>
> THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
> EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
> MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
> NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
> BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
> ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
> CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
> SOFTWARE.