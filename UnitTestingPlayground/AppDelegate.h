//
//  AppDelegate.h
//  UnitTestingPlayground
//
//  Created by Peter on 29/08/2013.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
