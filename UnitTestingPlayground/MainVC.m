//
//  mainVC.m
//  UnitTestingPlayground
//
//  Created by Peter on 29/08/2013.
//
//

#import "MainVC.h"

#define SEGUE_IDENT_MAINVC_TO_SECONDVC @"MainVC_to_SecondVC"
#define SEGUE_IDENT_MAINVC_BTN_TO_SECONDVC @"MainVC_btn_to_SecondVC"

@interface MainVC ()

@end

@implementation MainVC

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)gotoSecondVC:(id)sender
{
    [self performSegueWithIdentifier:SEGUE_IDENT_MAINVC_TO_SECONDVC sender:sender];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:SEGUE_IDENT_MAINVC_TO_SECONDVC]) {
        [[segue destinationViewController] setTitle:@"FROM ACTION"];
    } else if ([[segue identifier] isEqualToString:SEGUE_IDENT_MAINVC_BTN_TO_SECONDVC]) {
        [[segue destinationViewController] setTitle:@"FROM BUTTON"];
    }

}

@end
