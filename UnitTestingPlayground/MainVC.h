//
//  MainVC.h
//  UnitTestingPlayground
//
//  Created by Peter on 29/08/2013.
//
//

#import <UIKit/UIKit.h>

@interface MainVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btn_segue;
@property (weak, nonatomic) IBOutlet UIButton *btn_action;

- (IBAction)gotoSecondVC:(id)sender;

@end
