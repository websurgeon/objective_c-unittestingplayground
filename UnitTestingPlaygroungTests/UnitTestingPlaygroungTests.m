//
//  UnitTestingPlaygroungTests.m
//  UnitTestingPlaygroungTests
//
//  Created by Peter Barclay on 29/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

#import "MainVC.h"
#import <SenTestingKit/SenTestingKit.h>

#define HC_SHORTHAND
#import <OCHamcrestIOS/OCHamcrestIOS.h>

//#define MOCKITO_SHORTHAND
//#import <OCMockitoIOS/OCMockitoIOS.h>

#define SB_NAME_PHONE @"MainStoryboard_iPhone"

#define SB_IDENT_MAINVC @"MainVC"
#define SB_IDENT_SECONDVC @"SecondVC"

@interface UnitTestingPlaygroungTests : SenTestCase
@end

@implementation UnitTestingPlaygroungTests {
    UIStoryboard *storyboardPhone;
    UINavigationController *initVCPhone;
}

- (void)setUp
{
    [super setUp];

    storyboardPhone = [UIStoryboard storyboardWithName:SB_NAME_PHONE bundle:nil];
    initVCPhone = [storyboardPhone instantiateInitialViewController];
}

- (void)tearDown
{
    // Tear-down code here.
    
    [super tearDown];
}

- (void)testInitialViewControllerForPhoneIdiumIsNavigationController
{
    NSString *navClassName = NSStringFromClass(([UINavigationController class]));
    NSString *initVCClassName = NSStringFromClass([initVCPhone class]);
    assertThat(initVCClassName, equalTo(navClassName));
}

- (void)testViewControllerIsRootViewController
{
    MainVC *rootVC = (MainVC *)[initVCPhone topViewController];
    assertThat(NSStringFromClass([rootVC class]), equalTo(SB_IDENT_MAINVC));
}

- (void)testIdentifierMainVCExists
{
    assertThat([storyboardPhone instantiateViewControllerWithIdentifier:SB_IDENT_MAINVC], is(notNilValue()));
}

- (void)testIdentifierSecondVCExists
{
    assertThat([storyboardPhone instantiateViewControllerWithIdentifier:SB_IDENT_SECONDVC], is(notNilValue()));
}

@end
