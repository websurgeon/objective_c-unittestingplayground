//  MainVCTests.m
//  UnitTestingPlayground
//
//  Created by Peter Barclay on 29/08/2013.
//  Copyright (c) 2013 Peter Barclay. All rights reserved.
//

// Class under test
#import "MainVC.h"
#import "SecondVC.h"

#import <objc/runtime.h>
#import <SenTestingKit/SenTestingKit.h>

#define HC_SHORTHAND
#import <OCHamcrestIOS/OCHamcrestIOS.h>

#define MOCKITO_SHORTHAND
#import <OCMockitoIOS/OCMockitoIOS.h>

#define SB_NAME_PHONE @"MainStoryboard_iPhone"

#define SB_IDENT_MAINVC @"MainVC"
#define SB_IDENT_SECONDVC @"SecondVC"

#define SEGUE_IDENT_MAINVC_TO_SECONDVC @"MainVC_to_SecondVC"
#define SEGUE_IDENT_MAINVC_BTN_TO_SECONDVC @"MainVC_btn_to_SecondVC"

#define UIStoryboardPushSegue @"UIStoryboardPushSegue" // class name for hidden subclass of UIStoryboardSegue

// keys used for storing segue and sender passed into mainVCTests_prepareForSegue:sender:
static const char *storyboardSegueKey = "TestAssociatedStoryboardKey";
static const char *senderKey = "TestAssociatedSenderKey";

#pragma mark - MainVC Category Extension

@implementation MainVC (Testing)

// this category method is used to swap in place the classes real prepareForSegue:sender: method
- (void)mainVCTests_prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    objc_setAssociatedObject(self, storyboardSegueKey, segue, OBJC_ASSOCIATION_RETAIN);
    objc_setAssociatedObject(self, senderKey, sender, OBJC_ASSOCIATION_RETAIN);
}

@end

#pragma mark - MainVCTest

@interface MainVCTests : SenTestCase
@end

@implementation MainVCTests {
    SEL realPrepareForSegue;
    SEL testPrepareForSegue;
    UIStoryboard *storyboardPhone;
    UINavigationController *initVCPhone;
    MainVC *sutPhone;
}

#pragma mark - setup / teardown

- (void)setUp
{
    [super setUp];
    realPrepareForSegue = @selector(prepareForSegue:sender:);
    testPrepareForSegue = @selector(mainVCTests_prepareForSegue:sender:);
    
    storyboardPhone = [UIStoryboard storyboardWithName:SB_NAME_PHONE bundle:nil];
    initVCPhone = [storyboardPhone instantiateInitialViewController];
    sutPhone = (MainVC *)[(UINavigationController *)initVCPhone topViewController];
    [sutPhone view];
}

- (void)tearDown
{
    [super tearDown];
}

#pragma mark -

- (void)testSutPhoneIsMainVC
{
    assertThat(NSStringFromClass([sutPhone class]), is(equalTo(SB_IDENT_MAINVC)));
}

- (void)testTitleIsSet
{
    assertThat([sutPhone title], is(notNilValue()));
    assertThat([sutPhone title], isNot(equalTo(@"")));
}

- (void)testNavTitleIsSetToMainVCTitle
{
    assertThat([[sutPhone navigationItem] title], is(equalTo([sutPhone title])));
}

#pragma mark - Testing Button Action

- (void)testActionButtonShouldBeConnected
{
    assertThat([sutPhone btn_action], is(notNilValue()));
}

- (void)testActionButtonShouldHaveActionGotoSecondVC
{
    assertThat([[sutPhone btn_action] actionsForTarget:sutPhone forControlEvent:UIControlEventTouchUpInside], contains
               (@"gotoSecondVC:", nil));
}

#pragma mark - Testing Segue By Button

- (void)testSegueFromMainVCToSecondVCExists
{
    STAssertNoThrow([sutPhone performSegueWithIdentifier:SEGUE_IDENT_MAINVC_TO_SECONDVC sender:self],@"identifier not set correctly");
}


- (void)testActionShouldPerformSegueToSecondVC
{
    // swap out prepareForSegue:sender: with a test version of the method that will allow the segue and sender to be stored
    [MainVCTests swapInstanceMethodsForClass:[MainVC class]
                                    selector:realPrepareForSegue
                                 andSelector:testPrepareForSegue];

    // call action with the button as the sender arguement
    [sutPhone gotoSecondVC:[sutPhone btn_action]];
    
    // get segue object that is stored when mainVCTests_prepareForSegue:sender: is called
    UIStoryboardSegue *segue = objc_getAssociatedObject(sutPhone, storyboardSegueKey);
    id sender = objc_getAssociatedObject(sutPhone, senderKey);
    objc_removeAssociatedObjects(sutPhone);

    // check segue is the expected type
    assertThat(NSStringFromClass([segue class]), is(equalTo(UIStoryboardPushSegue)));

    // check destination is the expected class
    assertThatBool([[segue destinationViewController] isKindOfClass:[SecondVC class]], is(equalToBool(TRUE)));
    
    // check sender is the button that called the action
    assertThat(sender, is(equalTo([sutPhone btn_action])));
    
    // swap methods back to how they should be!
    [MainVCTests swapInstanceMethodsForClass:[MainVC class]
                                    selector:realPrepareForSegue
                                 andSelector:testPrepareForSegue];
}

- (void)testTitleIsSetByPrepareForSegueForActionSegue
{
    SecondVC *mockDestination = mock([SecondVC class]);
    
    UIStoryboardSegue *segue = [UIStoryboardSegue segueWithIdentifier:SEGUE_IDENT_MAINVC_TO_SECONDVC source:sutPhone destination:mockDestination performHandler:^{
        
    }];
    
    [sutPhone prepareForSegue:segue sender:[sutPhone btn_action]];
    
    [verify(mockDestination) setTitle:@"FROM ACTION"];
}

#pragma mark - Testing Segue by Button

- (void)testSegueButtonShouldBeConnected
{
    assertThat([sutPhone btn_segue], is(notNilValue()));
}

- (void)testSegueFromSegueButtonExists
{
    STAssertNoThrow([sutPhone performSegueWithIdentifier:SEGUE_IDENT_MAINVC_BTN_TO_SECONDVC sender:self],@"identifier not set correctly");
}


- (void)testSegueShouldBePerformedWhenSegueButtonIsTapped
{
    // swap out prepareForSegue:sender: with a test version of the method that will allow the segue and sender to be stored
    [MainVCTests swapInstanceMethodsForClass:[MainVC class]
                                    selector:realPrepareForSegue
                                 andSelector:testPrepareForSegue];

    // call action with the button as the sender arguement
    [[sutPhone btn_segue] sendActionsForControlEvents:UIControlEventTouchUpInside];
    
    // get segue object that is stored when mainVCTests_prepareForSegue:sender: is called
    UIStoryboardSegue *segue = objc_getAssociatedObject(sutPhone, storyboardSegueKey);
    id sender = objc_getAssociatedObject(sutPhone, senderKey);
    objc_removeAssociatedObjects(sutPhone);
    
    // check segue is the expected type
    assertThat(NSStringFromClass([segue class]), is(equalTo(UIStoryboardPushSegue)));
    
    // check destination is the expected class
    assertThatBool([[segue destinationViewController] isKindOfClass:[SecondVC class]], is(equalToBool(TRUE)));
    
    // check sender is the button that called the action
    assertThat(sender, is(equalTo([sutPhone btn_segue])));

    
    // swap out prepareForSegue:sender: with a test version of the method that will allow the segue and sender to be stored
    [MainVCTests swapInstanceMethodsForClass:[MainVC class]
                                    selector:realPrepareForSegue
                                 andSelector:testPrepareForSegue];

}

- (void)testTitleIsSetByPrepareForSegueForButtonSegue
{
    SecondVC *mockDestination = mock([SecondVC class]);
    
    UIStoryboardSegue *segue = [UIStoryboardSegue segueWithIdentifier:SEGUE_IDENT_MAINVC_BTN_TO_SECONDVC source:sutPhone destination:mockDestination performHandler:^{
        
    }];
    
    [sutPhone prepareForSegue:segue sender:[sutPhone btn_segue]];
    
    [verify(mockDestination) setTitle:@"FROM BUTTON"];
}

#pragma mark - Helper Methods

+ (void)swapInstanceMethodsForClass:(Class)cls selector:(SEL)sel1 andSelector:(SEL)sel2 {
    Method method1 = class_getInstanceMethod(cls, sel1);
    Method method2 = class_getInstanceMethod(cls, sel2);
    method_exchangeImplementations(method1, method2);
}


@end
